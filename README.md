# "HELLO WORLD" PACKAGE

This package is called "Zeeworld" and contains a function called "hello".
The function is empty and does not take in any arguments.
The package is available on GitLab, in a repository called Package_repo.
To install the package,
   - make sure you have devtools installed on your computer.
   - For the install options, choose install_gitlab since the package is on GitLab
   - 

